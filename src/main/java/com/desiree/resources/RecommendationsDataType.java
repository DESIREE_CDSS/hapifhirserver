package com.desiree.resources;

import org.hl7.fhir.instance.model.StringType;
import org.hl7.fhir.instance.model.Type;
import org.hl7.fhir.instance.model.api.ICompositeType;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.DatatypeDef;
import ca.uhn.fhir.util.ElementUtil;

@DatatypeDef(name="RecommendationsDataType")
public class RecommendationsDataType extends Type implements ICompositeType {

	private static final long serialVersionUID = 1L;

	@Child(name="recommendation", order = 0, min = 1, max = 1)
	private StringType recommendation;
	
	@Child(name = "source", order = 1, min = 1, max = 1)
	private StringType source;
	
	@Child(name = "strenght", order = 2, min = 1, max = 1)
	private StringType strenght;
	
	@Child(name = "usage", order = 3, min = 1, max = 1)
	private StringType usage;
	
	public StringType getRecommendation() {
		return recommendation;
	}
	public StringType getSource() {
		return source;
	}
	public StringType getStrenght() {
		return strenght;
	}
	public StringType getUsage() {
		return usage;
	}
	
	public RecommendationsDataType setRecommendation(StringType theRecommendation) {
		recommendation = theRecommendation;
		return this;
		
	}
	public RecommendationsDataType setSource(StringType theSource) {
		source = theSource;
		return this;
	}
	public RecommendationsDataType setStrenght(StringType theStrenght) {
		strenght = theStrenght;
		return this;
		
	}
	public RecommendationsDataType setUsage(StringType theUsage) {
		usage = theUsage;
		return this;
	}
	
	@Override
	public boolean isEmpty() {
		return ElementUtil.isEmpty(recommendation, source, strenght, usage);
	}
	
	@Override
	protected RecommendationsDataType typedCopy() {
		RecommendationsDataType retVal = new RecommendationsDataType();
		super.copyValues(retVal);
		retVal.recommendation = recommendation;
		retVal.source = source;
		retVal.strenght = strenght;
		retVal.usage = usage;
		return retVal;
	}

	
}
