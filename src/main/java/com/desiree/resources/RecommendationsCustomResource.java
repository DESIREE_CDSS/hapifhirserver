package com.desiree.resources;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.instance.model.DomainResource;
import org.hl7.fhir.instance.model.ResourceType;

import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.util.ElementUtil;

@ResourceDef(name="RecommendationsCustomResource", profile = "http://hl7.org/fhir/profiles/recommendations-custom-resource")
public class RecommendationsCustomResource extends DomainResource{

	private static final long serialVersionUID = 1L;

	@Child(name="referencePatient", min=1, max=1, order=0)
	private ResourceReferenceDt referencePatient;
	
	@Child(name="recommendations", min=0, max=Child.MAX_UNLIMITED, order=1)
	private List<RecommendationsDataType> recommendations;
	

	@Override
	public FhirVersionEnum getStructureFhirVersionEnum() {
		return FhirVersionEnum.DSTU2;
	}

	@Override
	public boolean isEmpty() {
		return ElementUtil.isEmpty(referencePatient, recommendations);
	}

	public ResourceReferenceDt getReferencePatient() {
		return referencePatient;
	}

	public void setReferencePatient(ResourceReferenceDt referencePatient) {
		this.referencePatient = referencePatient;
	}

	public List<RecommendationsDataType> getRecommendations() {
		if(recommendations == null){
			recommendations = new ArrayList<>();
		}
		return recommendations;
	}

	public void setRecommendations(List<RecommendationsDataType> recommendations) {
		this.recommendations = recommendations;
	}

	@Override
	public RecommendationsCustomResource copy() {
		RecommendationsCustomResource retVal = new RecommendationsCustomResource();
		super.copyValues(retVal);
		retVal.referencePatient = referencePatient;
		retVal.recommendations = recommendations;
		return retVal;
	}

	@Override
	public ResourceType getResourceType() {
		return null;
	}

}
