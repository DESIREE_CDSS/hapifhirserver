package com.desiree.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.uhn.fhir.model.api.ExtensionDt;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.valueset.AdministrativeGenderEnum;
import ca.uhn.fhir.model.dstu2.valueset.ContactPointUseEnum;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.model.primitive.DateDt;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.model.primitive.StringDt;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Update;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.desiree.model.MyPatient;

public class RestfulPatientResourceProvider implements IResourceProvider {

	private Map<Long, MyPatient> myPatients = new HashMap<Long, MyPatient>();
//	private Map<Long, PatientExercise1> myPatients = new HashMap<Long, PatientExercise1>();
	
	/** Constructor */
	public RestfulPatientResourceProvider() {
		MyPatient pat1 = new MyPatient();		
		pat1.addIdentifier().setSystem("http://acme.com/MRNs").setValue("7000135");		
		pat1.addName().addFamily("Simpson").addGiven("Homer").addGiven("J");		
		pat1.setAssignedArea(new CodeDt("Valencia"));
		pat1.setSocioeconomicalStatus(new CodeDt("Estado socioeconomico"));
		
//		PatientExercise1 pat1 = new PatientExercise1();
//		pat1.addIdentifier().setValue("amaia.ugarriza");
//		pat1.addName().addFamily("Ugarriza").addGiven("Amaia");
//		pat1.addTelecom().setValue("(03) 5555 6789").setUse(ContactPointUseEnum.HOME);
//		pat1.setGender(AdministrativeGenderEnum.FEMALE);
//		pat1.setBirthDate(new DateDt("1969-06-23"));
//		List<StringDt> listaLine = new ArrayList<>();
//		listaLine.add(new StringDt("3300 Washtenaw"));
//		pat1.addAddress().setLine(listaLine).setCity("Ann Harbor").setState("MI").setPostalCode("48104").setCountry("US");
//		pat1.setCounty(new ExtensionDt(false, "https://www.hl7.org/fhir/extension-us-core-county.html"));
		
		myPatients.put(1L, pat1);
	}
	
	/** All Resource Providers must implement this method */
	@Override
	public Class<? extends IResource> getResourceType() {
		return Patient.class;
	}

	/** Simple implementation of the "read" method */
//	@Read()
//	public PatientExercise1 read(@IdParam IdDt theId) {
//		PatientExercise1 retVal = myPatients.get(theId.getIdPartAsLong());
//		if (retVal == null) {
//			throw new ResourceNotFoundException(theId);
//		}
//		return retVal;
//	}
	@Read()
	public MyPatient read(@IdParam IdDt theId) {
		MyPatient retVal = myPatients.get(theId.getIdPartAsLong());
		if (retVal == null) {
			throw new ResourceNotFoundException(theId);
		}
		return retVal;
	}

	@Update
	public MethodOutcome updatePatient(@ResourceParam Patient thePatient) {
		MethodOutcome outcome = new MethodOutcome();
		if(thePatient!=null){
			outcome.setResource(thePatient);
			System.out.println();
			System.out.println("******************************************************");
			System.out.println("Los datos del paciente a almacenar son los siguientes:");
//			System.out.println("Identifier system: "+thePatient.getIdentifierFirstRep().getSystem());
//			System.out.println("Identifier value: "+thePatient.getIdentifierFirstRep().getSystemElement().getValue());
			System.out.println("Family: "+thePatient.getNameFirstRep().getFamilyFirstRep().getValue());
			System.out.println(thePatient.getNameFirstRep().getFamilyAsSingleString());
			System.out.println();
			System.out.println("******************************************************");
		}else{
			System.out.println("NO se ha podido actualizar el paciente enviado.");
		}
		return outcome;		
	}
	
}
