package com.desiree.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.instance.model.StringType;

import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.composite.ResourceReferenceDt;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Update;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.desiree.resources.RecommendationsCustomResource;
import com.desiree.resources.RecommendationsDataType;

public class RecommendationsCustomResourceProvider implements IResourceProvider {

	private Map<Long, RecommendationsCustomResource> myRecommendations = new HashMap<>();
	
	/** Constructor */
	public RecommendationsCustomResourceProvider() {
		
		RecommendationsCustomResource recommendations = new RecommendationsCustomResource();
		
		Patient pat = new Patient();
		pat.setId(new IdDt(1L));
		ResourceReferenceDt reference = new ResourceReferenceDt(pat);
		recommendations.setReferencePatient(reference);
		
		List<RecommendationsDataType> recommendationList = new ArrayList<>();
		RecommendationsDataType rec = new RecommendationsDataType();
		rec.setRecommendation(new StringType("SIB-56"));
		rec.setSource(new StringType("ONK"));
		rec.setStrenght(new StringType("78%"));
		rec.setUsage(new StringType("82%"));
		recommendationList.add(rec);
		
		rec = new RecommendationsDataType();
		rec.setRecommendation(new StringType("HIPO-40"));
		rec.setSource(new StringType("ONK"));
		rec.setStrenght(new StringType("67%"));
		rec.setUsage(new StringType("55%"));
		recommendationList.add(rec);
		
		recommendations.setRecommendations(recommendationList);
		
		myRecommendations.put(1L, recommendations);
	}
	
	/** All Resource Providers must implement this method */
	@Override
	public Class<? extends IResource> getResourceType() {
		return (Class<? extends IResource>) RecommendationsCustomResource.class;
	}

	/** Simple implementation of the "read" method */
	@Read()
	public RecommendationsCustomResource read(@IdParam IdDt theId) {
		RecommendationsCustomResource retVal = myRecommendations.get(theId.getIdPartAsLong());
		if (retVal == null) {
			throw new ResourceNotFoundException(theId);
		}
		return retVal;
	}
	
//	@Create
//	public void getRecommendations(@ResourceParam RecommendationsCustomResource theScenarioA){
//		if(theScenarioA!=null){
//			
//		}
//	}
	
	@Update
	public MethodOutcome updatePatient(@ResourceParam RecommendationsCustomResource theScenarioA) {
		MethodOutcome outcome = new MethodOutcome();
		if(theScenarioA!=null){
			outcome.setResource(theScenarioA);
//			System.out.println("Socioeconomical status: "+theScenarioA.getSocioeconomicalstatus().getValue());
//			System.out.println("Assigned area: "+theScenarioA.getAssignedarea().getValue());
//			System.out.println(theScenarioA.getFamilyhistory().size());
//			if(theScenarioA.getFamilyhistory().size()>0){
//				System.out.println("FAMILY HISTORY");
//				System.out.println("--------------");
//				for(FamilyHistoryDataType type: theScenarioA.getFamilyhistory()){
//					System.out.println("Relative: "+ type.getRelative().asStringValue() );
//					System.out.println("Age: "+ type.getAge().asStringValue() );
//					System.out.println("Cancer: "+ type.getCancer().asStringValue() );
//					System.out.println();
//				}
//				System.out.println("***************************");
//			}
		}else{
			System.out.println("NO se ha podido actualizar el scenario del paciente enviado.");
		}
		return outcome;		
	}

}
