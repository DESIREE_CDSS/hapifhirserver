package com.desiree.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.resource.ValueSet;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.desiree.model.ValueSetList;

public class RestfulValueSetProvider implements IResourceProvider {

	private Map<Object, ValueSet> myValueSets = new HashMap<>();

	/** Constructor */
	public RestfulValueSetProvider() {
		
		ValueSetList valueSetList = new ValueSetList();
		myValueSets = valueSetList.getValueSets();
		
	}
	
	/** All Resource Providers must implement this method */
	@Override
	public Class<? extends IResource> getResourceType() {
		return ValueSet.class;
	}

	/**
	 * Get all value sets
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@Search()
	public List<ValueSet> getValueSets() {
		List<ValueSet> list = new ArrayList<ValueSet>();
		Iterator it = myValueSets.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry e = (Map.Entry)it.next();
			list.add((ValueSet) e.getValue());
		}
		return list;
	}
	
	/**
	 * Get a value set by ID
	 * @param theId
	 * @return
	 */
	@Read()
	public ValueSet read(@IdParam IdDt theId) {
//		ValueSet retVal = myValueSets.get(theId.getIdPartAsLong());
		ValueSet retVal = myValueSets.get(theId.getIdPart());
		if (retVal == null) {
			throw new ResourceNotFoundException(theId);
		}
		return retVal;
	}

	/**
	 * Search value sets by name
	 * @param theName
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@Search()
	public ValueSet search(@RequiredParam(name="name") StringParam theName) {
		ValueSet retVal = null;
		Iterator it = myValueSets.entrySet().iterator();
		ValueSet value;
		while(it.hasNext()){
			Map.Entry e = (Map.Entry) it.next();
			value = (ValueSet) e.getValue();
			if(value.getIdentifier().getValue().equals(theName.getValue())){
				retVal = value;
			}
		}
		return retVal;
	}
}
