package com.desiree.providers;

import org.hl7.fhir.instance.model.api.IBaseResource;

import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Bundle.Entry;
import ca.uhn.fhir.model.dstu2.resource.FamilyMemberHistory;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.dstu2.resource.RiskAssessment;
import ca.uhn.fhir.model.dstu2.valueset.BundleTypeEnum;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;

public class RestfulBundleResourceProvider implements IResourceProvider {

	private Bundle bundle;
	
	public RestfulBundleResourceProvider(){
		bundle = new Bundle();
		bundle.setType(BundleTypeEnum.TRANSACTION);
		
	}

	@Override
	public Class<? extends IBaseResource> getResourceType() {
		return Bundle.class;
	}
	
	@Create
	public MethodOutcome postBundle(@ResourceParam Bundle theBundle) {
		System.out.println("");
		System.out.println("");
		System.out.println("******************************");
		System.out.println("HAPI FHIR SERVER");
		System.out.println("******************************");
		System.out.println("");
		MethodOutcome outcome = new MethodOutcome();
		if (theBundle != null) {
			IResource recurso;
			for(Entry entrada: theBundle.getEntry()){
				recurso = entrada.getResource();
				if(recurso instanceof Patient){
					System.out.println("Is a patient");
					Patient pac = (Patient) recurso;
					System.out.println("System: "+pac.getIdentifier().get(0).getSystem());
					System.out.println("Value: "+pac.getIdentifier().get(0).getValue());
					System.out.println();
					
				}
				if(recurso instanceof FamilyMemberHistory){
					System.out.println("Family History resource");
					System.out.println("");
				}
				if(recurso instanceof RiskAssessment){
					System.out.println("Risk assesment resource");
					System.out.println("");
				}
				if(recurso instanceof Observation){
					System.out.println("Observation resource");
					Observation obs = (Observation) recurso;
					System.out.println(obs.getSubject().getReference().getValue());
					System.out.println();
				}
				
			}
		}
		System.out.println("");
		return outcome;
	}
}
