package com.desiree.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.NarrativeDt;
import ca.uhn.fhir.model.dstu2.resource.ValueSet;
import ca.uhn.fhir.model.dstu2.resource.ValueSet.CodeSystem;
import ca.uhn.fhir.model.dstu2.resource.ValueSet.CodeSystemConcept;
import ca.uhn.fhir.model.dstu2.valueset.ConformanceResourceStatusEnum;
import ca.uhn.fhir.model.dstu2.valueset.NarrativeStatusEnum;
import ca.uhn.fhir.model.primitive.DateTimeDt;
import ca.uhn.fhir.model.primitive.IdDt;

public class ValueSetList {

	private Map<Object, ValueSet> myValueSets = new HashMap<>();
	
	public ValueSetList(){
		patientRelativeValueSet();
		cancerLocationValueSet();
		genderValueSet();
	}
	
	public Map<Object, ValueSet> getValueSets(){
		return this.myValueSets;
	}
	
	private void patientRelativeValueSet(){
		ValueSet value = new ValueSet();
		
		NarrativeDt text = new NarrativeDt();
		text.setStatus(NarrativeStatusEnum.GENERATED);
		text.setDiv("Value set for the patient relative data");
		value.setText(text);
		
		value.setId(new IdDt("PatientRelative"));
		
		value.setUrl("http://localhost:8080/DesireFhirServices/fhir/ValueSet/PatientRelative");
		
		IdentifierDt identifier = new IdentifierDt();
		identifier.setSystem("http://localhost:8080/DesireFhirServices/fhir/ValueSet/PatientRelative")
			.setValue("PatientRelative");
		value.setIdentifier(identifier);
		
		value.setVersion("20161214");
		
		value.setName("Patient Relative-20161214");
		
		value.setStatus(ConformanceResourceStatusEnum.ACTIVE);
		
		value.setExperimental(true);
		
		value.setPublisher("Bilbomatica");
		
		value.setDate(new DateTimeDt(new Date()));
		
		value.setDescription("Patient Relative value set - 2016/12/14");
		
		CodeSystem codeSystem =  new CodeSystem();
		codeSystem.setSystem("http://desiree.eu/fhir/codesystems/valuesets");
		codeSystem.setVersion("0.0.1");
		codeSystem.setCaseSensitive(true);
		
		List<CodeSystemConcept> conceptList = new ArrayList<>();
		CodeSystemConcept concept = new CodeSystemConcept();
		concept.setCode("1").setDisplay("Mother").setDefinition("Patient mother");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("2").setDisplay("Maternal grandmother").setDefinition("Patient Maternal grandmother");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("3").setDisplay("Paternal grandmother").setDefinition("Patient Paternal grandmother");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("4").setDisplay("Sister").setDefinition("Patient Sister");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("5").setDisplay("Daughter").setDefinition("Patient Daughter");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("6").setDisplay("Collateral family").setDefinition("Patient Collateral family");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("7").setDisplay("Father").setDefinition("Patient Father");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("8").setDisplay("Brother").setDefinition("Patient Brother");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("9").setDisplay("Grandfather").setDefinition("Patient Grandfather");
		conceptList.add(concept);
		codeSystem.setConcept(conceptList);
		value.setCodeSystem(codeSystem);
		
		myValueSets.put(value.getId().getValue(), value);
	}
	
	private void cancerLocationValueSet(){
		ValueSet value = new ValueSet();
		
		NarrativeDt text = new NarrativeDt();
		text.setStatus(NarrativeStatusEnum.GENERATED);
		text.setDiv("Value set for the cancer location data");
		value.setText(text);
		value.setId(new IdDt("CancerLocation"));
		value.setUrl("http://localhost:8080/DesireFhirServices/fhir/ValueSet/CancerLocation");
		
		IdentifierDt identifier = new IdentifierDt();
		identifier.setSystem("http://localhost:8080/DesireFhirServices/fhir/ValueSet/CancerLocation")
			.setValue("CancerLocation");
		value.setIdentifier(identifier);
		
		value.setVersion("20170113");
		value.setName("Cancer Location-20170113");
		value.setStatus(ConformanceResourceStatusEnum.ACTIVE);
		value.setExperimental(true);
		value.setPublisher("Bilbomatica");
		value.setDate(new DateTimeDt(new Date()));
		value.setDescription("Cancer location value set - 2017/01/13");
		
		CodeSystem codeSystem =  new CodeSystem();
		codeSystem.setSystem("http://desiree.eu/fhir/codesystems/valuesets");
		codeSystem.setVersion("0.0.1");
		codeSystem.setCaseSensitive(true);
		
		List<CodeSystemConcept> conceptList = new ArrayList<>();
		CodeSystemConcept concept = new CodeSystemConcept();
		concept.setCode("1").setDisplay("Breast").setDefinition("Breast cancer");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("2").setDisplay("Ovary").setDefinition("Ovary cancer");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("3").setDisplay("Other").setDefinition("cancer in other location");
		conceptList.add(concept);
		codeSystem.setConcept(conceptList);
		value.setCodeSystem(codeSystem);
		myValueSets.put(value.getId().getValue(), value);
	}
	
	private void genderValueSet(){
		ValueSet value = new ValueSet();
		
		NarrativeDt text = new NarrativeDt();
		text.setStatus(NarrativeStatusEnum.GENERATED);
		text.setDiv("Value set for patient gender");
		value.setText(text);
		value.setId(new IdDt("Gender"));
		value.setUrl("http://localhost:8080/DesireFhirServices/fhir/ValueSet/Gender");
		
		IdentifierDt identifier = new IdentifierDt();
		identifier.setSystem("http://localhost:8080/DesireFhirServices/fhir/ValueSet/Gender")
			.setValue("Gender");
		value.setIdentifier(identifier);
		
		value.setVersion("20170113");
		value.setName("Gender-20170113");
		value.setStatus(ConformanceResourceStatusEnum.ACTIVE);
		value.setExperimental(true);
		value.setPublisher("Bilbomatica");
		value.setDate(new DateTimeDt(new Date()));
		value.setDescription("Cancer location value set - 2017/01/13");
		
		CodeSystem codeSystem =  new CodeSystem();
//		codeSystem.setSystem("http://desiree.eu/fhir/codesystems/valuesets");
		codeSystem.setVersion("0.0.1");
		codeSystem.setCaseSensitive(true);
		
		List<CodeSystemConcept> conceptList = new ArrayList<>();
		CodeSystemConcept concept = new CodeSystemConcept();
		concept.setCode("1").setDisplay("Female").setDefinition("Female patient");
		conceptList.add(concept);
		concept = new CodeSystemConcept();
		concept.setCode("2").setDisplay("Male").setDefinition("Male patient");
		conceptList.add(concept);
		codeSystem.setConcept(conceptList);
		value.setCodeSystem(codeSystem);
		myValueSets.put(value.getId().getValue(), value);
	}
}
