package com.desiree.model;

import java.util.List;

import ca.uhn.fhir.model.api.IElement;
import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.Extension;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.CodeDt;
import ca.uhn.fhir.util.ElementUtil;

@ResourceDef(name="Patient")
public class MyPatient extends Patient{

	/* *****************************
	 * Fields
	 * *****************************/
	private static final long serialVersionUID = 1L;

	/**
	 * This is a basic extension, with a DataType value (in this case, String)
	 */
	@Description(shortDefinition = "Contains the assigned area of the patient")
	@Extension(url = "http://fhir#assignedArea", isModifier = false, definedLocally = true)
	@Child(name = "assignedArea")
	private CodeDt myAssignedArea;
	
	@Description(shortDefinition = "Contains the socioeconomical status of the patient")
	@Extension(url = "http://fhir#socioeconomicalStatus", isModifier = false, definedLocally = true)
	@Child(name = "socioeconomicalStatus")
	private CodeDt mySocioeconomicalStatus;
	
	
	/* *****************************
	 * Getters and setters
	 * *****************************/
	
	public CodeDt getAssignedArea() {
		if (myAssignedArea == null) {
			myAssignedArea = new CodeDt();
		}
		return myAssignedArea;
	}

	public void setAssignedArea(CodeDt theAssignedArea) {
		myAssignedArea = theAssignedArea;
	}
	
	public CodeDt getSocioeconomicalStatus() {
		if (mySocioeconomicalStatus == null) {
			mySocioeconomicalStatus = new CodeDt();
		}
		return mySocioeconomicalStatus;
	}

	public void setSocioeconomicalStatus(CodeDt theSocioeconomicalStatus) {
		mySocioeconomicalStatus = theSocioeconomicalStatus;
	}
	
	
	/* *****************************
	 * Boilerplate methods- Hopefully these will be removed or made optional
	 * in a future version of HAPI but for now they need to be added to all block
	 * types. These two methods follow a simple pattern where a utility method from
	 * ElementUtil is called and all fields are passed in.
	 * *****************************/
	@Override
	public <T extends IElement> List<T> getAllPopulatedChildElementsOfType(Class<T> theType) {
		return ElementUtil.allPopulatedChildElements(theType, super.getAllPopulatedChildElementsOfType(theType), myAssignedArea, mySocioeconomicalStatus);
	}

	@Override
	public boolean isEmpty() {
        return super.isEmpty() && ElementUtil.isEmpty(myAssignedArea, mySocioeconomicalStatus);
	}
}
