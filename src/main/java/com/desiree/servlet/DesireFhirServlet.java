package com.desiree.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.hl7.fhir.instance.hapi.validation.FhirInstanceValidator;

import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import com.desiree.providers.RestfulBundleResourceProvider;
import com.desiree.providers.RestfulPatientResourceProvider;
import com.desiree.providers.RecommendationsCustomResourceProvider;
import com.desiree.providers.RestfulValueSetProvider;
import com.desiree.resources.RecommendationsCustomResource;
import com.desiree.resources.RecommendationsDataType;

//@WebServlet("/fhir/*")
public class DesireFhirServlet extends RestfulServer{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void initialize() throws ServletException {
		
		/*
		 * The servlet defines any number of resource providers, and
		 * configures itself to use them by calling
		 * setResourceProviders()
		 */
		List<IResourceProvider> resourceProviders = new ArrayList<IResourceProvider>();
		resourceProviders.add(new RestfulPatientResourceProvider());
		resourceProviders.add(new RestfulBundleResourceProvider());
		resourceProviders.add(new RecommendationsCustomResourceProvider());
		resourceProviders.add(new RestfulValueSetProvider());
		this.setResourceProviders(resourceProviders);
		
		getFhirContext().registerCustomType(RecommendationsCustomResource.class);
		getFhirContext().registerCustomType(RecommendationsDataType.class);
		
		/* This just means to use Content-Types which are not technically
		 * FHIR compliant if a browser is detected (so that they display
		 * nicely for testing) */
//		this.setUseBrowserFriendlyContentTypes(true);
		
		/*
		 * This server interceptor causes the server to return nicely
		 * formatter and coloured responses instead of plain JSON/XML if
		 * the request is coming from a browser window. It is optional,
		 * but can be nice for testing.
		 */
		registerInterceptor(new ResponseHighlighterInterceptor());
		
//		getFhirContext().newJsonParser().setPrettyPrint(true);
//		this.setDefaultResponseEncoding(EncodingEnum.JSON);
		this.setDefaultPrettyPrint(true);
		
	}
	
}
